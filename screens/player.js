import React from 'react';
import { View,Text, StyleSheet } from 'react-native';
import TheoPlayerViewScreen from './TheoPlayerViewScreen';

const PlayerScreen = ({navigation,route})=>{
    
    const {auth} = route.params;
    return(
        <View style={styles.container}>
            <TheoPlayerViewScreen customData={auth} />
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1
    }
})
export default PlayerScreen;