import React from 'react';
import { Dimensions, StyleSheet, View, Platform, ScrollView } from 'react-native';
import THEOplayerView from './THEOplayerView'

export default class TheoPlayerViewScreen extends React.Component {

    render() {
        /*
          Problem on android fullscreen change with theoplayer scaling when ScrollView component is set
        */
        let BaseComponent = View;
        console.log('vust', this.props.customData);
        /*
          If there are scaling issues during the change of the fullscreen remove 'aspectRatio' & set player height
        */
        let playerStyle = {
            ...styles.player,
        };

        if (Platform.OS === 'android') {
            playerStyle.width = Math.floor(Dimensions.get('window').width);
        } else {
            BaseComponent = ScrollView;
        }
        let testSource = {
            sources: [{
                type: 'application/x-mpegurl',
                src: 'https://cdn.theoplayer.com/video/big_buck_bunny/big_buck_bunny.m3u8',
            }],
            poster: 'https://cdn.theoplayer.com/video/big_buck_bunny/poster.jpg',
        }
        if (Platform.OS === 'android') {
            playerStyle.width = Math.floor(Dimensions.get('window').width);
            testSource = {
                sources: [{
                    type: 'application/dash+xml',
                    src: 'https://roots-video-output.s3.amazonaws.com/1_Anaagat/Default/DASH/1_Anaagat.mpd',
                    drm: {
                        integration: 'keyos',
                        customdata: this.props.customData,
                        widevine: {
                            licenseAcquisitionURL: "https://wv-keyos.licensekeyserver.com/"
                        }


                    }
                }],
                poster: 'https://cdn.theoplayer.com/video/big_buck_bunny/poster.jpg',
            }


        } else {
            BaseComponent = ScrollView;
            testSource = {
                sources: [{
                    src: "https://roots-video-output.s3.amazonaws.com/1_Anaagat/Default/HLS/1_Anaagat.m3u8",
                    type: "application/x-mpegurl",
                    drm: {
                        integration: 'keyos',
                        customdata: this.props.customData,
                        fairplay: {
                           licenseAcquisitionURL: "https://fp-keyos.licensekeyserver.com/getkey",
                            certificateURL: "https://fp-keyos.licensekeyserver.com/cert/2F516149A7C2D181C20451863AE8A3E3F2788C5B.der"
                        }
                    }

                }],
            }

        }

        return (
            <BaseComponent style={styles.containerBase}>
                <View style={styles.container}>
                    <THEOplayerView
                        style={playerStyle}
                        fullscreenOrientationCoupling={true}
                        autoplay={true}
                        source={testSource}
                        

                    />
                </View>
            </BaseComponent>
        );
    }
}

const styles = StyleSheet.create({
    containerBase: {
        flex: 1,
    },

    container: {
        flex: 1,
    },

    player: {
        backgroundColor: "black",
        aspectRatio: 1.7,
    }
});