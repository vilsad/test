import React from 'react';
import { useState } from 'react';
import { Button, View, Text, StyleSheet, Platform } from 'react-native';

const loginURL = "http://useruatapi.rootsvideo.com/user/login";
const authURL = "http://52.3.37.6/movies/getdrm/"

const HomeScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    return (
        <View style={styles.container}>
            {
                loading &&
                <Text>Loading..</Text>
            }
            <Button style={styles.play} title="Play" onPress={() => {
                console.log('clicked');
                fetch(loginURL, {
                    method: 'POST',
                    headers: {
                        accept: 'application/json',
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        "UserName": "damu@gmail.com",
                        "Password": "123456"
                    })
                }).then((response) => response.json())
                    .then((loginResult) => {
                        console.log('Login',loginResult);
                        fetch(authURL + (Platform.OS == 'ios' ? 'IOS' : 'ANDROID'), {
                            method: 'GET',
                            headers: {
                                accept: 'application/json',
                                authorization: 'Bearer ' + loginResult.token
                            }
                        })
                        .then((response)=>response.json())
                        .then((authToken)=>{
                            console.log('Token',authToken);
                            navigation.navigate('Player', { auth: authToken.authXML })
                        })
                    })
            }} ></Button>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        display: 'flex',
        alignItems: 'center',
        alignContent: 'center',
        flex: 1
    },
    play: {
        justifyContent: 'center'
    }
})
export default HomeScreen;